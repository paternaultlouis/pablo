# Pablo

Classes LaTeX personnelles pour la rédaction de mes cours et devoirs (présentés [sur mon site web](http://ababsurdo.fr/cours/), et disponibles dans [mes dépôts git](https://forge.apps.education.fr/paternaultlouis/)).

## Installation

Ces classes sont principalement une compilation des paquets que j'utilise souvent, et de « raccourcis » de commandes fréquentes. Elles ne présentent donc aucun intérêt particulier pour un autre usage que la compilation de mes cours et devoirs. C'est la raison pour laquelle vous ne les trouverez pas sur [CTAN](http://ctan.org/).

Pour les utiliser :

- [Téléchargez les fichiers](https://forge.apps.education.fr/paternaultlouis/pablo/-/archive/main/pablo-main.zip), ou clonez ce dépôt.
- Décompressez l'archive.
- Installez les.
  - Version simple : Placez les fichiers `sty` nécessaires dans le même dossier que le fichier `tex` à compiler.
  - Version compliquée (mais plus propre et pérenne) : Placez les fichiers `sty` dans un des dossiers dans lequel LaTeX va chercher les paquets.

## Licence

Ces fichiers sont publiées sous les licences suivantes, au choix :

* [LaTeX Project Public License](http://latex-project.org/lppl/), version 1.3c ou supérieure ;
* [Do What The Fuck You Want To Public License](http://www.wtfpl.net/) ou sa traduction française la [Licence Publique Rien À Branler](http://sam.zoy.org/lprab/) ;
* [CC0 1.0 universel](https://creativecommons.org/publicdomain/zero/1.0/deed.fr) ;
* versé dans le domaine public, dans les législations qui le permettent (ce qui n'est pas le cas de la France).
