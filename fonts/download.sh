#!/bin/bash

set -e
set -x

WGETOPTIONS="--timeout=30 --tries=2"

FONTSDIR=$(cd $(dirname $0) && pwd)/truetype

TEMPDIR=$(mktemp --directory)
function cleanup {
  echo "Suppression du répertoire temporaire"
  rm -r ${TEMPDIR}
}
trap cleanup EXIT
cd $TEMPDIR

# CASIO
wget $WGETOPTIONS https://edu.casio.com/education/fontset/dl/GraphicSeriesFontSet.zip && \
unzip GraphicSeriesFontSet.zip && \
cp GraphicSeriesFontSet/*{ttf,TTF} ${FONTSDIR}

wget $WGETOPTIONS https://edu.casio.com/education/fontset/dl/MSSeriesFontSet.zip && \
unzip MSSeriesFontSet.zip && \
cp "CASIO MS01.ttf" ${FONTSDIR}/CASIO_MS01.ttf

# Numworks
wget $WGETOPTIONS https://www.numworks.com/shared/pages/blog/numworks-keys-font/numworks-keys-font-351dbb06.zip && \
unzip numworks-keys-font-351dbb06.zip && \
cp *ttf ${FONTSDIR}

# TI82
wget $WGETOPTIONS https://education.ti.com/download/en/ed-tech/2B7835B24E6948A59848F67BD93BC795/5BB590A0519E4C02BDA84AC3E12C23BA/ti82keys.exe && \
yes | unzip ti82keys.exe && \
cp *TTF ${FONTSDIR}

# TI83
wget $WGETOPTIONS https://education.ti.com/download/en/ed-tech/781AF09FD41C471995EBC0153112B027/05332A946BD34070B7DF0FFCA6B3919B/ti83keys.exe && \
yes | unzip ti83keys.exe && \
cp *TTF ${FONTSDIR}

# TI 89
wget $WGETOPTIONS https://education.ti.com/download/en/ed-tech/E5285DC893154C42B2E2D45AA3D11051/A22E11884D734BE69659B1F18ED71996/ti89keys.exe && \
yes | unzip ti89keys.exe && \
cp *ttf ${FONTSDIR}

# Noto
wget $WGETOPTIONS https://github.com/googlefonts/noto-emoji/raw/main/fonts/NotoColorEmoji.ttf && \
mv NotoColorEmoji.ttf ${FONTSDIR}
