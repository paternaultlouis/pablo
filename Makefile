FONTS=\
			fonts/truetype/numworks-keys-bold.ttf fonts/truetype/numworks-keys-regular.ttf \
			fonts/truetype/NotoColorEmoji.ttf

all: $(FONTS)

# Polices numworks
.ONESHELL:
tmp/numworks-keys-%.ttf: tmp/numworks.zip
	# Extraction de l'archive
	unzip -o $< -d $(@D) $(@F)

fonts/truetype/numworks-keys-%.ttf: tmp/numworks-keys-%.ttf
	# Copie du fichier
	cp -f $< $@

tmp/numworks.zip:
	# Téléchargement de l'archive
	mkdir -p tmp
	wget --output-document $@ https://www.numworks.com/shared/pages/blog/numworks-keys-font/numworks-keys-font-351dbb06.zip

# Polices noto-emoji
fonts/truetype/NotoColorEmoji.ttf:
	wget --output-document $@ https://github.com/googlefonts/noto-emoji/raw/main/fonts/NotoColorEmoji.ttf
