#!/bin/bash

which vim >/dev/null && export EDITOR="vim"
export TEXINPUTS="$TEXINPUTS:$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)"
export TEXINPUTS="$TEXINPUTS:$(cd $(dirname ${BASH_SOURCE[0]})/latex && pwd)"
export TEXMFHOME="$TEXMFHOME:$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)"

function texloop() {
	\ls $@ | entr lualatex --halt-on-error $1
}

alias pif="pdfimpose cards"
